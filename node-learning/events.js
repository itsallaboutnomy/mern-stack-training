const EventEmitter = require ('events');
const eventEmitter = new EventEmitter();
eventEmitter.on('tutorial', (num1,num2)=>{
    console.log(num1+num2);
})
eventEmitter.emit('tutorial',1,2);

class Person extends EventEmitter{

    constructor(name){
    super();
    this._name  = name;

    }
    get name(){
        return this._name;
    }
}

let nauman = new Person('Nauman');
let helloBoy = new Person('Hello-Boy');

nauman.on('name',()=>{
    console.log('My Name is ' + nauman.name)
})
nauman.emit('name');

somia.on('name',()=>{
    console.log('My Name is ' + somia.name)
})
somia.emit('name');