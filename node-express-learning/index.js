const express = require('express'); //import module
// the above constant will return a function called express();
const app = express();
// this express method retrun an object which has a bunch of methods

app.get('/',(req,res)=>{

    res.send('Hello world');
});

app.get('/example',(req,res)=>{

    res.send('hittings example');
});

app.get('/example/:name/:age',(req,res)=>{

    console.log(req.query);
    res.send(req.params.name +":" + req.params.age );


});

app.listen('3000');